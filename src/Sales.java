/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author informatics
 */
public class Sales {
    int Bill_id;
    int Bill_totalcost;
    String Bill_date;

    public int getBill_id() {
        return Bill_id;
    }

    public void setBill_id(int Bill_id) {
        this.Bill_id = Bill_id;
    }

    public int getBill_totalcost() {
        return Bill_totalcost;
    }

    public void setBill_totalcost(int Bill_totalcost) {
        this.Bill_totalcost = Bill_totalcost;
    }

    public String getBill_date() {
        return Bill_date;
    }

    public void setBill_date(String Bill_date) {
        this.Bill_date = Bill_date;
    }

    public Sales() {
    }

    public Sales(int Bill_id, int Bill_totalcost, String Bill_date) {
        this.Bill_id = Bill_id;
        this.Bill_totalcost = Bill_totalcost;
        this.Bill_date = Bill_date;
    }
    
}
