
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author informatics
 */
public class Product {
   int Product_id;
   String Product_type;
   String Product_brand;
   String Product_name;
   double Product_size;
   String Product_color;
   int Product_cost;
   int Product_quantity;

    public Product() {
        
    }

    public Product(int Product_id, String Product_type, String Product_brand, String Product_name, double Product_size, String Product_color, int Product_cost, int Product_quantity) {
        this.Product_id = Product_id;
        this.Product_type = Product_type;
        this.Product_brand = Product_brand;
        this.Product_name = Product_name;
        this.Product_size = Product_size;
        this.Product_color = Product_color;
        this.Product_cost = Product_cost;
        this.Product_quantity = Product_quantity;
    }
    

    public int getProduct_id() {
        return Product_id;
    }

    public String getProduct_type() {
        return Product_type;
    }

    public String getProduct_brand() {
        return Product_brand;
    }

    public String getProduct_name() {
        return Product_name;
    }

    public double getProduct_size() {
        return Product_size;
    }

    public String getProduct_color() {
        return Product_color;
    }

    public int getProduct_cost() {
        return Product_cost;
    }

    public int getProduct_quantity() {
        return Product_quantity;
    }
    
   
   
}


