/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import sneakersshop.SneakersShop;

/**
 *
 * @author Panu Rungkaew
 */
public class MemberDao {
    public  static boolean insert(Member member){
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO Member (\n" +
"                      Member_id,\n"+
"                      Member_name,\n" +
"                      Member_surname,\n" +
"                      Member_age,\n" +
"                      Member_idcard,\n" +
"                      Member_sex,\n" +
"                      Member_tel,\n" ;
            stm.execute(String.format(sql,member.getMember_id(),member.getMember_name(),member.getMember_surname(),member.getMember_age()
                    ,member.getMember_idcard(),member.getMember_sex(),member.getMember_tel()));
             Database.close();
                return true;
        } catch (SQLException ex) {
            Logger.getLogger(MemberDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return true;
    }
    public  static boolean update(Member member){
        String sql = "UPDATE Member\n" +
                     "SET \n" +
                     " Member_id = '%S',\n" +
                     " Member_name = '%S',\n" +
                     " Member_surname = '%S',\n" +
                     " Member_age = '%d',\n" +
                     " Member_idcard = '%S',\n" +
                     " Member_sex = '%S',\n" +
                     " Member_tal = '%S',\n" +
                     " WHERE Member_id = %d;";
        Connection conn = Database.connect();
        Statement stm;
        try {
            stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql,member.getMember_id()
                ,member.getMember_name()
                ,member.getMember_surname()
                ,member.getMember_age()
                ,member.getMember_idcard()
                ,member.getMember_sex()
                ,member.getMember_tel()
                ));
            Database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(MemberDao.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        Database.close();
        return true;
    }
     public static boolean delete(Member member){
          String sql = "DELETE FROM Member  WHERE Member_id = %d " ;
          Connection conn = Database.connect();
         
        try {
             Statement stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql,member.getMember_id()));
            Database.close();
        } catch (SQLException ex) {
            Logger.getLogger(MemberDao.class.getName()).log(Level.SEVERE, null, ex);
        }
          Database.close();
        return true;
    }
     public static ArrayList<Member> getMembers(){
         ArrayList<Member> list = new ArrayList<Member>();
         Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT Member_id,\n" +
"                         Member_name,\n" +
"                         Member_surname,\n" +
"                         Member_age,\n" +
"                         Member_sex,\n" +
"                         Member_idcard,\n" +
"                         Member_tel\n" +
"                         FROM Member";
            ResultSet rs = stm.executeQuery(sql);
            while(rs.next()){
                //System.out.println(rs.getInt("Staff_id")+" "+rs.getString("Staff_username"));
                Member member = toObject(rs);
                list.add(member);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(SneakersShop.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    private static Member toObject(ResultSet rs) throws SQLException {
        Member member;
        member = new Member();
        member.setMember_id(rs.getInt("Member_id"));
        member.setMember_name(rs.getString("Member_naame"));
        member.setMember_surname(rs.getString("Member_surname"));
        member.setMember_age(rs.getInt("Member_age"));
        member.setMember_idcard(rs.getString("Member_idcard"));
        member.setMember_sex(rs.getString("Member_sex"));
        member.setMember_tel(rs.getString("Member_tel"));
        
        return member;
    }
     public static Member getMember(int Memberid){
          String sql = "SELECT Member_id,\n" +
"                         Member_name,\n" +
"                         Member_surname,\n" +
"                         Member_age,\n" +
"                         Member_sex,\n" +
"                         Member_idcard,\n" +
"                         Member_tel\n" +
"                         FROM Member WHERE Member_id = %d";
         Connection conn = Database.connect();
         
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, Memberid));
            if(rs.next()){
                Member member = toObject(rs);
                Database.close();
                return member;
            }
        } catch (SQLException ex) {
            Logger.getLogger(MemberDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }
}
