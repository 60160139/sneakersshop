/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

/**
 *
 * @author informatics
 */
public class BillDetail {
    int Bucket_id;
    int Bucket_number;
    int Bucket_totalcost;
    int Product_id;

    public BillDetail() {
        this.Bucket_id = -1;
    }

    public BillDetail(int Bucket_id, int Bucket_number, int Bucket_totalcost, int Product_id) {
        this.Bucket_id = Bucket_id;
        this.Bucket_number = Bucket_number;
        this.Bucket_totalcost = Bucket_totalcost;
        this.Product_id = Product_id;
    }
    

    public int getBucket_id() {
        return Bucket_id;
    }

    public void setBucket_id(int Bucket_id) {
        this.Bucket_id = Bucket_id;
    }

    public int getBucket_number() {
        return Bucket_number;
    }

    public void setBucket_number(int Bucket_number) {
        this.Bucket_number = Bucket_number;
    }

    public int getBucket_totalcost() {
        return Bucket_totalcost;
    }

    public void setBucket_totalcost(int Bucket_totalcost) {
        this.Bucket_totalcost = Bucket_totalcost;
    }

    public int getProduct_id() {
        return Product_id;
    }

    public void setProduct_id(int Product_id) {
        this.Product_id = Product_id;
    }
    
}
