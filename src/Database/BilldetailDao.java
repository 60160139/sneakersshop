/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import sneakersshop.SneakersShop;

/**
 *
 * @author Panu Rungkaew
 */
public class BilldetailDao {
    public  static boolean insert(BillDetail billdetail){
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO Bill (\n" +
"                         Bucket_id,\n" +
"                         Bucket_number,\n" +
"                         Bucket_totalcost,\n" +
"                         Product_id";
            stm.execute(String.format(sql,billdetail.getBucket_id(),billdetail.getBucket_number(),billdetail.getBucket_totalcost(),billdetail.getProduct_id()));
             Database.close();
                return true;
        } catch (SQLException ex) {
            Logger.getLogger(BilldetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return true;
    }
    public  static boolean update(BillDetail billdeail){
        String sql = "UPDATE BillDetail\n" +
                     "SET \n" +
                     "Bucket_id,\n" +
"                     Bucket_number,\n" +
"                     Bucket_totalcost,\n" +
"                     Product_id"+
"                     WHERE Bucket_id = %d;";
        Connection conn = Database.connect();
        Statement stm;
        try {
            stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql,billdeail.getBucket_id()
                ,billdeail.getBucket_number()
                ,billdeail.getBucket_totalcost()
                ,billdeail.getProduct_id()));
            Database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(BilldetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        Database.close();
        return true;
    }
     public static boolean delete(BillDetail billdtail){
          String sql = "DELETE FROM BillDetail   WHERE Bucket_id = %d " ;
          Connection conn = Database.connect();
         
        try {
             Statement stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql,billdtail.getBucket_id()));
            Database.close();
        } catch (SQLException ex) {
            Logger.getLogger(BilldetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
          Database.close();
        return true;
    }
     public static ArrayList<BillDetail> getBillDetails(){
         ArrayList<BillDetail> list = new ArrayList<BillDetail>();
         Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT Bucket_id,\n" +
"                         Bucket_number,\n" +
"                         Bucket_totalcost,\n" +
"                         Product_id\n" +
"                         FROM BillDetail";
            ResultSet rs = stm.executeQuery(sql);
            while(rs.next()){
                //System.out.println(rs.getInt("Staff_id")+" "+rs.getString("Staff_username"));
                BillDetail billdetail = toObject(rs);
                list.add(billdetail);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(SneakersShop.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    private static BillDetail toObject(ResultSet rs) throws SQLException {
        BillDetail billdetail;
        billdetail = new BillDetail();
        billdetail.setBucket_id(rs.getInt("Bucket_id"));
        billdetail.setBucket_number(rs.getInt("Bucket_number"));
        billdetail.setBucket_totalcost(rs.getInt("Bucket_totalcost"));
        billdetail.setProduct_id(rs.getInt("Product_id"));
        return billdetail;
    }
     public static BillDetail getBillDetail(int BillDetailid){
          String sql = "SELECT Bucket_id,\n" +
"                         Bucket_number,\n" +
"                         Bucket_totalcost,\n" +
"                         Product_id\n" +
"                         FROM BillDetail WHERE Bucket_id = %d";
         Connection conn = Database.connect();
         
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, BillDetailid));
            if(rs.next()){
                BillDetail billdetail = toObject(rs);
                Database.close();
                return billdetail;
            }
        } catch (SQLException ex) {
            Logger.getLogger(BillDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }
}
