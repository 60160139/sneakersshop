/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import sneakersshop.SneakersShop;

/**
 *
 * @author informatics
 */
public class ProductDao {

    public static boolean insert(Product product) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO Product (\n"
                    + "                        Product_quantity,\n"
                    + "                        Product_cost,\n"
                    + "                        Product_color,\n"
                    + "                        Product_size,\n"
                    + "                        Product_name,\n"
                    + "                        Product_brand,\n"
                    + "                        Product_type,\n"
                    + "                        Product_id\n"
                    + "                    )\n"
                    + "                    ;";
            stm.execute(String.format(sql, product.getProduct_type(), product.getProduct_brand(),
                    product.getProduct_name(), product.getProduct_size(), product.getProduct_color(),
                    product.getProduct_cost(), product.getProduct_quantity()));
            Database.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return true;
    }

    public static boolean update(Product product) {
        String sql = "UPDATE Product\n"
                + "   SET Product_id = 'Product_id',\n"
                + "       Product_type = 'Product_type',\n"
                + "       Product_brand = 'Product_brand',\n"
                + "       Product_name = 'Product_name',\n"
                + "       Product_size = 'Product_size',\n"
                + "       Product_color = 'Product_color',\n"
                + "       Product_cost = 'Product_cost',\n"
                + "       Product_quantity = 'Product_quantity'\n"
                + " WHERE Product_brand = 'Nike' AND \n"
                + "       Product_size = 11.5 AND \n"
                + "       Product_color = 'Green' AND \n"
                + "       Product_cost = 2000 AND \n"
                + "       Product_type = 'Sneakers' AND \n"
                + "       Product_id = 1 AND \n"
                + "       Product_quantity = 2 AND \n"
                + "       Product_name = 'AriMax97';";
        Connection conn = Database.connect();
        Statement stm;
        try {
            stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql, product.getProduct_type(),
                    product.getProduct_brand(),
                    product.getProduct_name(),
                    product.getProduct_size(),
                    product.getProduct_color(),
                    product.getProduct_cost(),
                    product.getProduct_quantity()));
            Database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return true;
    }

    public static boolean delete(Product product) {
        String sql = "DELETE FROM Product  WHERE Product_id = %d ";
        Connection conn = Database.connect();

        try {
            Statement stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql, product.getProduct_id()));
            Database.close();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return true;
    }

    public static ArrayList<Product> getProducts() {
        ArrayList<Product> list = new ArrayList<>();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT Product_id,\n"
                    + "       Product_type,\n"
                    + "       Product_brand,\n"
                    + "       Product_name,\n"
                    + "       Product_size,\n"
                    + "       Product_color,\n"
                    + "       Product_cost,\n"
                    + "       Product_quantity\n"
                    + "  FROM Product\n"
                    + "WHERE Product_id";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Product product = toObject(rs);
                list.add(product);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(SneakersShop.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    private static Product toObject(ResultSet rs) throws SQLException {
        Product product;
        product = new Product();
        product.setProduct_id(rs.getInt("Product_id"));
        product.setProduct_type(rs.getString("Product_type"));
        product.setProduct_brand(rs.getString("Product_brand"));
        product.setProduct_name(rs.getString("Product_name"));
        product.setProduct_size(rs.getDouble("Product_size"));
        product.setProduct_color(rs.getString("Product_color"));
        product.setProduct_cost(rs.getInt("Product_cost"));
        product.setProduct_quantity(rs.getInt("Product_quantity"));
        return product;
    }

    public static Product getProduct(int Staffid) {
        String sql = "SELECT Product_id,\n"
                + "       Product_type,\n"
                + "       Product_brand,\n"
                + "       Product_name,\n"
                + "       Product_size,\n"
                + "       Product_color,\n"
                + "       Product_cost,\n"
                + "       Product_quantity\n"
                + "  FROM Product\n"
                + " WHERE Product_brand = 'Nike' AND \n"
                + "       Product_size = 11.5 AND \n"
                + "       Product_color = 'Green' AND \n"
                + "       Product_cost = 2000 AND \n"
                + "       Product_type = 'Sneakers' AND \n"
                + "       Product_id = 1 AND \n"
                + "       Product_quantity = 2 AND \n"
                + "       Product_name = 'AriMax97';";
        Connection conn = Database.connect();

        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, Staffid));
            if (rs.next()) {
                Product product = toObject(rs);
                Database.close();
                return product;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }
}
