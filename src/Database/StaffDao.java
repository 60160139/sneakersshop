/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import sneakersshop.SneakersShop;

/**
 *
 * @author Panu Rungkaew
 */
public class StaffDao {
    public  static boolean insert(Staff staff){
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO Staff (\n" +
"                      Staff_username,\n" +
"                      Staff_password,\n" +
"                      Staff_name,\n" +
"                      Staff_surname,\n" +
"                      Staff_age,\n" +
"                      Staff_idcard,\n" +
"                      Staff_sex,\n" +
"                      Staff_tel,\n" +
"                      Staff_type               \n" +
"                  )\n" +
"                  VALUES (\n" +
"                      '%S',\n" +
"                      '%S',\n" +
"                      '%S',\n" +
"                      '%S',\n" +
"                       %d,\n" +
"                      '%S',\n" +
"                      '%S',\n" +
"                      '%S',\n" +
"                      '%S'\n" +
"                  );"
;
            stm.execute(String.format(sql, staff.getStaff_username(),staff.getStaff_password()
                    ,staff.getStaff_name(),staff.getStaff_surname(),staff.getStaff_age()
                    ,staff.getStaff_idcard(),staff.getStaff_sex(),staff.getStaff_tel(),staff.getStaff_type()));
             Database.close();
                return true;
        } catch (SQLException ex) {
            Logger.getLogger(StaffDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return true;
    }
    public  static boolean update(Staff staff){
        String sql = "UPDATE Staff\n" +
                     "SET \n" +
                     " Staff_username = '%S',\n" +
                     " Staff_password = '%S',\n" +
                     " Staff_name = '%S',\n" +
                     " Staff_surname = '%S',\n" +
                     " Staff_age = '%d',\n" +
                     " Staff_idcard = '%S',\n" +
                     " Staff_sex = '%S',\n" +
                     " Staff_tel = '%S',\n" +
                     " Staff_type = '%S'\n" +
                     " WHERE Staff_id = %d;";
        Connection conn = Database.connect();
        Statement stm;
        try {
            stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql, staff.getStaff_username()
                ,staff.getStaff_password()
                ,staff.getStaff_name()
                ,staff.getStaff_surname()
                ,staff.getStaff_age()
                ,staff.getStaff_idcard()
                ,staff.getStaff_sex()
                ,staff.getStaff_tel()
                ,staff.getStaff_type(),
                staff.getStaff_id()
                ));
            Database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(StaffDao.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        Database.close();
        return true;
    }
     public static boolean delete(Staff staff){
          String sql = "DELETE FROM Staff  WHERE Staff_id = %d " ;
          Connection conn = Database.connect();
         
        try {
             Statement stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql,staff.getStaff_id()));
            Database.close();
        } catch (SQLException ex) {
            Logger.getLogger(StaffDao.class.getName()).log(Level.SEVERE, null, ex);
        }
          Database.close();
        return true;
    }
     public static ArrayList<Staff> getStaffs(){
         ArrayList<Staff> list = new ArrayList<Staff>();
         Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT Staff_id,\n" +
"                           Staff_username,\n" +
"                           Staff_password,\n" +
"                           Staff_name,\n" +
"                           Staff_surname,\n" +
"                           Staff_age,\n" +
"                           Staff_idcard,\n" +
"                           Staff_sex,\n" +
"                           Staff_tel,\n" +
"                           Staff_type\n" +
"                           FROM Staff";
            ResultSet rs = stm.executeQuery(sql);
            while(rs.next()){
                //System.out.println(rs.getInt("Staff_id")+" "+rs.getString("Staff_username"));
                Staff staff = toObject(rs);
                list.add(staff);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(SneakersShop.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

//      public static Staff getStaff(int id){
//         Connection conn = Database.connect();
//        try {
//            Statement stm = conn.createStatement();
//            String sql = "SELECT Staff_id,\n" +
//"                           Staff_username,\n" +
//"                           Staff_password,\n" +
//"                           Staff_name,\n" +
//"                           Staff_surname,\n" +
//"                           Staff_age,\n" +
//"                           Staff_idcard,\n" +
//"                           Staff_sex,\n" +
//"                           Staff_tel,\n" +
//"                           Staff_type\n" +
//"                           FROM Staff " +
//                    "   WHERE Staff_id = %d";
//            ResultSet rs = stm.executeQuery(String.format(sql, id));
//            if(rs.next()){
//                //System.out.println(rs.getInt("Staff_id")+" "+rs.getString("Staff_username"));
//                Staff staff = toObject(rs);
//                Database.close();
//                return staff;
//            }
//            return null;
//
//        } catch (SQLException ex) {
//            Logger.getLogger(SneakersShop.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        Database.close();
//        return null;
//    }

     
     
    private static Staff toObject(ResultSet rs) throws SQLException {
        Staff staff;
        staff = new Staff();
        staff.setStaff_id(rs.getInt("Staff_id"));
        staff.setStaff_username(rs.getString("Staff_username"));
        staff.setStaff_password(rs.getString("Staff_password"));
        staff.setStaff_name(rs.getString("Staff_name"));
        staff.setStaff_surname(rs.getString("Staff_surname"));
        staff.setStaff_age(rs.getInt("Staff_age"));
        staff.setStaff_idcard(rs.getString("Staff_idcard"));
        staff.setStaff_sex(rs.getString("Staff_sex"));
        staff.setStaff_tel(rs.getString("Staff_tel"));
        staff.setStaff_type(rs.getString("Staff_type"));
        return staff;
    }
     
    
    
    
    public static Staff getStaff(int Staffid){
          String sql = "SELECT Staff_id,\n" +
"                           Staff_username,\n" +
"                           Staff_password,\n" +
"                           Staff_name,\n" +
"                           Staff_surname,\n" +
"                           Staff_age,\n" +
"                           Staff_idcard,\n" +
"                           Staff_sex,\n" +
"                           Staff_tel,\n" +
"                           Staff_type\n" +
"                           FROM Staff WHERE Staff_id = %d";
         Connection conn = Database.connect();
         
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, Staffid));
            if(rs.next()){
                Staff staff = toObject(rs);
                Database.close();
                return staff;
            }
        } catch (SQLException ex) {
            Logger.getLogger(StaffDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }
    
    public static boolean chackStaff(String staffusSername,String staffPassword){
          String sql = "SELECT Staff_id,\n" +
"                           Staff_username,\n" +
"                           Staff_password,\n" +
"                           FROM Staff WHERE Staff_id = %d";
         Connection conn = Database.connect();
         boolean uesr = false;
         boolean pass = false;
        try {
            Statement stm = conn.createStatement();
             ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                String id = rs.getString("Staff_username");
                String password = rs.getString("Staff_password");
 
                if (staffusSername.equals(id)) {
                    uesr =  true;
                    
                    if (staffPassword.equals(password)) {
                        pass = true;
                    } else {
                        pass =false;
                    }
                } else {
                    uesr= false;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(StaffDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        if(uesr&&pass){
            return true;
        }else{
            return false;
        }
        
    }
    
}
