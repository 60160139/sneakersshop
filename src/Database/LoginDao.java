/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import sneakersshop.SneakersShop;

/**
 *
 * @author Panu Rungkaew
 */
public class LoginDao {
    public  static boolean insert(Login login){
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO Login (\n" +
"                      Login_id,\n" +
"                      login_in,\n" +
"                      login_out,\n" +
"                      Staff_id,\n" +
"                  )\n" +
"                  VALUES (\n" +
"                      '%d',\n" +
"                      '%S',\n" +
"                      '%S',\n" +
"                      '%d',\n" +
"                  );"
;
            stm.execute(String.format(sql, login.getLogin_id(),login.getLogin_in()
                    ,login.getLogin_out(),login.getStaff_id()));
             Database.close();
                return true;
        } catch (SQLException ex) {
            Logger.getLogger(StaffDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return true;
    }
    public  static boolean update(Login login){
        String sql = "UPDATE Staff\n" +
                     "SET \n" +
                     " Login_id = '%d',\n" +
                     " Login_in = '%S',\n" +
                     " Login_out = '%S',\n" +
                     " Staff_id = '%d',\n" +
                     " WHERE Login_id = %d;";
        Connection conn = Database.connect();
        Statement stm;
        try {
            stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql, login.getLogin_id()
                ,login.getLogin_in()
                ,login.getLogin_out()
                ,login.getStaff_id()
                ));
            Database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(StaffDao.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        Database.close();
        return true;
    }
//     public static Login getLogin(int loginId){
//         ArrayList<Login> list = new ArrayList<Login>();
//         Connection conn = Database.connect();
//        try {
//            Statement stm = conn.createStatement();
//            String sql = "SELECT Login_id,\n" +
//"                           Staff_id,\n" +
//"                           FROM Login";
//            ResultSet rs = stm.executeQuery(sql);
//            while(rs.next()){
//                //System.out.println(rs.getInt("Staff_id")+" "+rs.getString("Staff_username"));
//                Login login = toObject(rs);
//                list.add(login);
//                Database.close();
//                return login;
//            }
//            return null;
//        } catch (SQLException ex) {
//            Logger.getLogger(SneakersShop.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        Database.close();
//        return null;
//    }
     
    
    
    public static ArrayList<Login> getLogin(){
         ArrayList<Login> list = new ArrayList<Login>();
         Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT Login_id,\n" +
"                           Login_in,\n" +
"                           Login_out,\n" +
"                           Staff_id,\n" +
"                           FROM Login";
            ResultSet rs = stm.executeQuery(sql);
            while(rs.next()){
                //System.out.println(rs.getInt("Staff_id")+" "+rs.getString("Staff_username"));
                Login login = toObject(rs);
                list.add(login);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(SneakersShop.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    private static Login toObject(ResultSet rs) throws SQLException {
        Login login;
        login = new Login();
        login.setLogin_id(rs.getInt("Login_id"));
        login.setLogin_in(rs.getString("Login_in"));
        login.setLogin_out(rs.getString("Login_out"));
        login.setStaff_id(rs.getInt("Staff_id"));
        return login;
    }
     public static Login getLogin(int Loginid){
          String sql = "SELECT Login_id,\n" +
"                           Login_in,\n" +
"                           Login_out,\n" +
"                           Staff_id,\n" +
"                           FROM Login WHERE Login_id = %d";
         Connection conn = Database.connect();
         
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, Loginid));
            if(rs.next()){
                Login login = toObject(rs);
                Database.close();
                return login;
            }
        } catch (SQLException ex) {
            Logger.getLogger(StaffDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }
     
     public static ArrayList<Login> getLoginId(){
         ArrayList<Login> list = new ArrayList<Login>();
         Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT Login_id,\n" +
"                           FROM Login";
            ResultSet rs = stm.executeQuery(sql);
            while(rs.next()){
                //System.out.println(rs.getInt("Staff_id")+" "+rs.getString("Staff_username"));
                Login login = toObject(rs);
                list.add(login);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(SneakersShop.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }
}
