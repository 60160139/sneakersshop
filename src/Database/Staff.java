/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

/**
 *
 * @author Panu Rungkaew
 */
public class Staff {
    int Staff_id;
    String Staff_username;
    String Staff_password;
    String Staff_name;
    String Staff_surname;
    int Staff_age;
    String Staff_idcard;
    String Staff_sex;
    String Staff_tel;
    String Staff_type;

    public Staff() {
        this.Staff_id = -1;
    }

    public Staff(int Staff_id, String Staff_username, String Staff_password, String Staff_name, String Staff_surname, int Staff_age, String Staff_idcard, String Staff_sex, String Staff_tel, String Staff_type) {
        this.Staff_id = Staff_id;
        this.Staff_username = Staff_username;
        this.Staff_password = Staff_password;
        this.Staff_name = Staff_name;
        this.Staff_surname = Staff_surname;
        this.Staff_age = Staff_age;
        this.Staff_idcard = Staff_idcard;
        this.Staff_sex = Staff_sex;
        this.Staff_tel = Staff_tel;
        this.Staff_type = Staff_type;
    }

    public int getStaff_id() {
        return Staff_id;
    }

    public void setStaff_id(int Staff_id) {
        this.Staff_id = Staff_id;
    }

    public String getStaff_username() {
        return Staff_username;
    }

    public void setStaff_username(String Staff_username) {
        this.Staff_username = Staff_username;
    }

    public String getStaff_password() {
        return Staff_password;
    }

    public void setStaff_password(String Staff_password) {
        this.Staff_password = Staff_password;
    }

    public String getStaff_name() {
        return Staff_name;
    }

    public void setStaff_name(String Staff_name) {
        this.Staff_name = Staff_name;
    }

    public String getStaff_surname() {
        return Staff_surname;
    }

    public void setStaff_surname(String Staff_surname) {
        this.Staff_surname = Staff_surname;
    }

    public int getStaff_age() {
        return Staff_age;
    }

    public void setStaff_age(int Staff_age) {
        this.Staff_age = Staff_age;
    }

    public String getStaff_idcard() {
        return Staff_idcard;
    }

    public void setStaff_idcard(String Staff_idcard) {
        this.Staff_idcard = Staff_idcard;
    }

    public String getStaff_sex() {
        return Staff_sex;
    }

    public void setStaff_sex(String Staff_sex) {
        this.Staff_sex = Staff_sex;
    }

    public String getStaff_tel() {
        return Staff_tel;
    }

    public void setStaff_tel(String Staff_tel) {
        this.Staff_tel = Staff_tel;
    }

    public String getStaff_type() {
        return Staff_type;
    }

    public void setStaff_type(String Staff_type) {
        this.Staff_type = Staff_type;
    }

    @Override
    public String toString() {
        return "Staff{" + "Staff_id=" + Staff_id + ", Staff_username=" + Staff_username + ", Staff_password=" + Staff_password + ", Staff_name=" + Staff_name + ", Staff_surname=" + Staff_surname + ", Staff_age=" + Staff_age + ", Staff_idcard=" + Staff_idcard + ", Staff_sex=" + Staff_sex + ", Staff_tel=" + Staff_tel + ", Staff_type=" + Staff_type + '}';
    }
    
    
}
