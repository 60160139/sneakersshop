package Database;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author informatics
 */
public class Login {
    private int Login_id;
    private String  Login_in;
    private String Login_out;
    private int Staff_id;
    
     public Login() {
        this.Staff_id = -1;
    }
    
     public Login(int Login_id, String Login_in,String Login_out,int Starff_id) {
        this.Login_id = Login_id;
        this.Login_in = Login_in;
        this.Login_out = Login_out;
        this.Staff_id = Staff_id;
    }


    public int getLogin_id() {
        return Login_id;
    }

    public void setLogin_id(int Login_id) {
        this.Login_id = Login_id;
    }

    public String getLogin_in() {
        return Login_in;
    }

    public void setLogin_in(String Login_in) {
        this.Login_in = Login_in;
    }

    public String getLogin_out() {
        return Login_out;
    }

    public void setLogin_out(String Login_out) {
        this.Login_out = Login_out;
    }

    public int getStaff_id() {
        return Staff_id;
    }

    public void setStaff_id(int Staff_id) {
        this.Staff_id = Staff_id;
    }

     @Override
    public String toString() {
        return "Staff{" + "Login_in=" + Login_id + ", Login_in=" + Login_in + ", Login_out=" + Login_out + ", staff_id=" + Staff_id + '}';
    }
    
    
}
