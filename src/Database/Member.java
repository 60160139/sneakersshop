/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

/**
 *
 * @author informatics
 */
public class Member {
    int Member_id;
    String Member_name;
    String Member_surname;
    int Member_age;
    String Member_sex;
    String Member_idcard;
    String Member_tel;

    public Member() {
         this.Member_id = -1;
    }

    public Member(int Member_id, String Member_name, String Member_surname, int Member_age, String Member_sex, String Member_idcard, String Member_tel) {
        this.Member_id = Member_id;
        this.Member_name = Member_name;
        this.Member_surname = Member_surname;
        this.Member_age = Member_age;
        this.Member_sex = Member_sex;
        this.Member_idcard = Member_idcard;
        this.Member_tel = Member_tel;
    }
        
    
    
    public int getMember_id() {
        return Member_id;
    }

    public void setMember_id(int Member_id) {
        this.Member_id = Member_id;
    }

    public String getMember_name() {
        return Member_name;
    }

    public void setMember_name(String Member_name) {
        this.Member_name = Member_name;
    }

    public String getMember_surname() {
        return Member_surname;
    }

    public void setMember_surname(String Member_surname) {
        this.Member_surname = Member_surname;
    }

    public int getMember_age() {
        return Member_age;
    }

    public void setMember_age(int Member_age) {
        this.Member_age = Member_age;
    }

    public String getMember_sex() {
        return Member_sex;
    }

    public void setMember_sex(String Member_sex) {
        this.Member_sex = Member_sex;
    }

    public String getMember_idcard() {
        return Member_idcard;
    }

    public void setMember_idcard(String Member_idcard) {
        this.Member_idcard = Member_idcard;
    }

    public String getMember_tel() {
        return Member_tel;
    }

    public void setMember_tel(String Member_tel) {
        this.Member_tel = Member_tel;
    }
 
}
