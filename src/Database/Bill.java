/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

/**
 *
 * @author informatics
 */
public class Bill {
    int Bill_id;
    int Bill_totalcost;
    String Bill_date;
    int Bucket_id;
    int Login_id;
    int Member_id;

    public Bill() {
         this.Bill_id = -1;
    }

    public Bill(int Bill_id, int Bill_totalcost, String Bill_date, int Bucket_id, int Login_id, int Member_id) {
        this.Bill_id = Bill_id;
        this.Bill_totalcost = Bill_totalcost;
        this.Bill_date = Bill_date;
        this.Bucket_id = Bucket_id;
        this.Login_id = Login_id;
        this.Member_id = Member_id;
    }
    

    public int getBill_id() {
        return Bill_id;
    }

    public void setBill_id(int Bill_id) {
        this.Bill_id = Bill_id;
    }

    public int getBill_totalcost() {
        return Bill_totalcost;
    }

    public void setBill_totalcost(int Bill_totalcost) {
        this.Bill_totalcost = Bill_totalcost;
    }

    public String getBill_date() {
        return Bill_date;
    }

    public void setBill_date(String Bill_date) {
        this.Bill_date = Bill_date;
    }

    public int getBucket_id() {
        return Bucket_id;
    }

    public void setBucket_id(int Bucket_id) {
        this.Bucket_id = Bucket_id;
    }

    public int getLogin_id() {
        return Login_id;
    }

    public void setLogin_id(int Login_id) {
        this.Login_id = Login_id;
    }

    public int getMember_id() {
        return Member_id;
    }

    public void setMember_id(int Member_id) {
        this.Member_id = Member_id;
    }
    
}
