
import java.util.ArrayList;
import Database.Product;
import javax.swing.table.AbstractTableModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author informatics
 */
public class ProductTableModel extends AbstractTableModel{
    String[] columnNames = {"ID","Type","Brand","Name","Color","Size","Cost","Quantity"};
    ArrayList<Product> ProductList = new ArrayList<Product>();
    @Override
    public String getColumnName(int column){
        return columnNames[column];
    }
    @Override
    public int getRowCount() {
         return ProductList.size();
    }

    @Override
    public int getColumnCount() { 
        return columnNames.length;
      
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Product product = ProductList.get(rowIndex);
        if(product == null)
            return  "";
        switch(columnIndex){
            case 0 : return product.getProduct_id();
            case 1 : return product.getProduct_type();
            case 2 : return product.getProduct_brand();
            case 3 : return product.getProduct_name();
            case 4 : return product.getProduct_color();
            case 5 : return product.getProduct_size();
            case 6 : return product.getProduct_cost();
            case 7 : return product.getProduct_quantity();

        }
        return "";
    }

    public void setData(ArrayList<Product> ProductList){
        this.ProductList = ProductList;
        fireTableDataChanged();
                
    }
    
}
